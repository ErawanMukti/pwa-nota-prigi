import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Create from '@/components/Create'
import Show from '@/components/Show'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/create',
      name: 'create',
      component: Create
    },
    {
      path: '/show/:id',
      name: 'show',
      component: Show
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
