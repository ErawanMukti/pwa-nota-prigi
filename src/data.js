export default {
  nota: [
    {
      'id': 0,
      'tanggal': '2019-01-14',
      'jam': '08:00:00',
      'customer': 'Erawan',
      'berat_netto': 3504,
      'total': 9636000
    },
    {
      'id': 1,
      'tanggal': '2019-01-14',
      'jam': '09:00:00',
      'customer': 'Abi',
      'berat_netto': 3504,
      'total': 9636000
    },
    {
      'id': 2,
      'tanggal': '2019-01-14',
      'jam': '10:00:00',
      'customer': 'Bagas',
      'berat_netto': 1641,
      'total': 4512750
    },
    {
      'id': 3,
      'tanggal': '2019-01-14',
      'jam': '11:00:00',
      'customer': 'Agung',
      'berat_netto': 6885,
      'total': 18933750
    },
    {
      'id': 4,
      'tanggal': '2019-01-14',
      'jam': '12:00:00',
      'customer': 'Abi',
      'berat_netto': 3504,
      'total': 9636000
    }
  ]
}
