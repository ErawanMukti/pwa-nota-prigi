// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import Vuefire from 'vuefire'
import VueResource from 'vue-resource'
import currency from 'v-currency-field'

import 'vuetify/dist/vuetify.min.css'
import firebase from './services/firebase'
import 'v-currency-field/dist/index.css'

Vue.use(Vuetify)
Vue.use(Vuefire)
Vue.use(VueResource)
Vue.use(currency)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  firebase: {
    nota: firebase.database.ref('nota').orderByChild('created_at'),
    customer: firebase.database.ref('customer').orderByChild('created_at')
  },
  router,
  template: '<App/>',
  components: { App }
})
